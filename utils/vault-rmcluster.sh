#!/bin/bash
set -euo pipefail
common_path_prefix="yaook"
cluster="$1"
cluster_path="$common_path_prefix/$cluster"

echo 'This will REMOVE all data stored in vault related to the cluster'
echo
echo "    ${cluster}"
echo
echo
read -r -p "ARE YOU SURE? (type capital 'yes')" response
case "$response" in
    YES)
        ;;
    *)
        echo 'User consent not given, bailing out.' >&2
        exit 2
        ;;
esac

vault secrets disable "$cluster_path/kv"
vault secrets disable "$cluster_path/ssh-ca"

vault list -format json "auth/$common_path_prefix/nodes/role" | jq -r --arg cluster "$cluster" '.[] | select(endswith(".node." + $cluster))' | xargs -I{} -- vault delete "auth/$common_path_prefix/nodes/role/"{}
