#!/bin/bash
set -euo pipefail
repopath="$1"
mkdir -p "$repopath"
cd "$repopath"
git init .
git submodule add https://gitlab.com/yaook/k8s managed-k8s
mkdir -p config
cp --no-clobber managed-k8s/templates/config.template.toml config/config.toml
