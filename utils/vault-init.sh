#!/bin/bash
set -euo pipefail
common_path_prefix="yaook"
common_policy_prefix="yaook"
nodes_approle_name="$common_path_prefix/nodes"
nodes_approle_path="auth/$nodes_approle_name"

if ! vault read -field="$nodes_approle_name/" sys/auth >/dev/null; then
    vault auth enable -path="$nodes_approle_name" approle
fi
nodes_approle_accessor=$(vault read -field="$nodes_approle_name/" -format=json sys/auth | jq -r .accessor)

vault policy write "$common_policy_prefix/k8s-master" - <<EOF
path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/data/k8s/join-key" {
    capabilities = ["create", "update"]
}

path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/data/k8s/etc/*" {
    capabilities = ["create", "update", "delete"]
}
EOF

vault policy write "$common_policy_prefix/k8s-node" - <<EOF
path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/data/k8s/join-key" {
    capabilities = ["read"]
}

path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/data/k8s/etc" {
    capabilities = ["read"]
}

path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/data/k8s/etc/*" {
    capabilities = ["read"]
}

path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/metadata/k8s/etc" {
    capabilities = ["list"]
}

path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/kv/metadata/k8s/etc/*" {
    capabilities = ["list"]
}
EOF


vault policy write "$common_policy_prefix/node" - <<EOF
path "$common_path_prefix/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.yaook_deployment }}/ssh-ca/sign/{{ identity.entity.aliases.$nodes_approle_accessor.metadata.role_name }}" {
    capabilities = ["create", "update"]
}
EOF

vault policy write "$common_policy_prefix/group-controller" - <<EOF
path "$common_path_prefix/+/ssh-ca/roles/+" {
    capabilities = ["delete"]
}

path "$common_path_prefix/+/ssh-ca/roles/+" {
    capabilities = ["create", "update"]
    required_parameters = ["key_type", "ttl", "allow_host_certificates", "allow_bare_domains", "allowed_domains", "algorithm_signer"]
    allowed_parameters = {
        "key_type" = ["ca"],
        "ttl" = [],
        "allow_host_certificates" = ["true"],
        "allow_bare_domains" = ["true"],
        "allowed_domains" = [],
        "algorithm_signer" = [],
    }
}

path "$nodes_approle_path/role/+" {
    capabilities = ["create", "update"]
    required_parameters = ["token_ttl", "token_max_ttl", "token_policies", "token_no_default_policy", "token_type"]
    allowed_parameters = {
        "token_ttl" = [],
        "token_max_ttl" = ["1h"],
        "token_policies" = [
            "$common_policy_prefix/k8s-master,$common_policy_prefix/k8s-node,$common_policy_prefix/node",
            "$common_policy_prefix/k8s-node,$common_policy_prefix/node",
        ],
        "token_no_default_policy" = ["false"],
        "token_type" = ["service"],
    }
}

path "$nodes_approle_path/role/+" {
    capabilities = ["delete"]
}

path "$nodes_approle_path/role/+/role-id" {
    capabilities = ["read"]
}

path "$nodes_approle_path/role/+/secret-id" {
    capabilities = ["create", "update"]
    required_parameters = ["metadata"]
    allowed_parameters = {
        "metadata" = ["yaook_deployment=*"],
    }
    min_wrapping_ttl = "1h"
    max_wrapping_ttl = "3h"
}

path "$common_path_prefix/+/kv/data/ipmi/*" {
    capabilities = ["read"]
}
EOF
