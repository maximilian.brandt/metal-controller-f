#!/bin/bash
set -euo pipefail
cluster="$1"
node="$2"
control_plane="${3:-false}"

common_path_prefix="yaook"
common_policy_prefix="yaook"
cluster_path="yaook/$cluster"
sshca_path="$cluster_path/ssh-ca"
nodes_approle_path="auth/$common_path_prefix/nodes"

VAULT_TOKEN="$(vault token create -field=token -policy="$common_policy_prefix/group-controller" -ttl=1h)"
export VAULT_TOKEN

function revoke() {
    vault token revoke -self >/dev/null 2>/dev/null || true
}

trap revoke EXIT

policies="$common_policy_prefix/k8s-node,$common_policy_prefix/node"
if [ "$control_plane" = 'true' ]; then
    policies="$common_policy_prefix/k8s-master,$policies"
fi

node_approle_name="$node.$cluster"
node_approle_path="$nodes_approle_path/role/$node.$cluster"
node_sshca_path="$sshca_path/roles/$node_approle_name"

vault delete "$node_sshca_path" >/dev/null
vault write "$node_sshca_path" \
    key_type=ca \
    ttl=720h \
    allow_host_certificates=true \
    allow_bare_domains=true \
    allowed_domains="$node.nodes.$cluster" \
    algorithm_signer=rsa-sha2-512 >/dev/null
vault delete "$node_approle_path" >/dev/null
vault write "$node_approle_path" \
    token_ttl=10m \
    token_max_ttl=1h \
    token_policies="$policies" \
    token_no_default_policy=false \
    token_type=service >/dev/null

role_id="$(vault read -field=role_id "$node_approle_path/role-id")"
secret_id_wrapping_token="$(vault write -f -wrap-ttl=1h -field=wrapping_token "$node_approle_path/secret-id" metadata=yaook_deployment="$cluster")"

jq -R --slurp '(. / "\n") | {"role_id": .[0], "secret_id_wrapping_token": .[1]}' <<<"$role_id"$'\n'"$secret_id_wrapping_token"
