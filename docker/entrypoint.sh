#!/bin/bash
set -eu
cmd="$1"
shift
export PATH="/docker-run/:$PATH"
exec tini -- "$cmd" "$@"
