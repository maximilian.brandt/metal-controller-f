import contextlib
import io
import json
import pathlib
import os
import subprocess
import sys
import tempfile
import unittest
import unittest.mock
from unittest.mock import sentinel

import metal_controller.deploy as deploy
import metal_controller.model as model
import metal_controller.configdrive as configdrive


class Test_write_inventory(unittest.TestCase):
    @classmethod
    def _cycle(cls, nodes):
        buf = io.StringIO()
        deploy._write_inventory(buf, nodes)
        buf.seek(0, 0)
        return buf.getvalue()

    def test_empty(self):
        inventory = self._cycle({})
        self.assertIn("\n[all:vars]\non_openstack=False\n", inventory)
        self.assertIn("\n[k8s_nodes:children]\nmasters\nworkers\n", inventory)
        self.assertIn("\n[frontend:children]\nmasters\ngateways\n", inventory)
        self.assertIn("\n[gateways]\n", inventory)
        self.assertIn("\n[masters]\n", inventory)
        self.assertIn("\n[workers]\n", inventory)

    def test_mix_of_nodes_ipv4(self):
        nodes = {
            "node1": model.NodeInfo(
                ipv4="192.0.2.1",
                ipv6=None,
                is_control_plane=True,
            ),
            "node2": model.NodeInfo(
                ipv4="192.0.2.2",
                ipv6=None,
                is_control_plane=False,
            ),
            "node3.testdomain.fqdn.long": model.NodeInfo(
                ipv4="192.0.2.3",
                ipv6=None,
                is_control_plane=True,
            ),
            "node4.testdomain.fqdn.long": model.NodeInfo(
                ipv4="192.0.2.4",
                ipv6=None,
                is_control_plane=False,
            ),
        }

        inventory = self._cycle(nodes)

        self.assertIn(
            "\n[masters]\n"
            "node1 ansible_host=192.0.2.1 local_ipv4_address=192.0.2.1"
            " ansible_user=centos\n"
            "node3.testdomain.fqdn.long ansible_host=192.0.2.3"
            " local_ipv4_address=192.0.2.3"
            " ansible_user=centos\n",
            inventory,
        )

        self.assertIn(
            "\n[workers]\n"
            "node2 ansible_host=192.0.2.2 local_ipv4_address=192.0.2.2"
            " ansible_user=centos\n"
            "node4.testdomain.fqdn.long ansible_host=192.0.2.4"
            " local_ipv4_address=192.0.2.4"
            " ansible_user=centos\n",
            inventory,
        )

    def test_mix_of_nodes_dualstack(self):
        nodes = {
            "node1": model.NodeInfo(
                ipv4="192.0.2.1",
                ipv6="fd00::1",
                is_control_plane=True,
            ),
            "node2": model.NodeInfo(
                ipv4="192.0.2.2",
                ipv6="fd00::2",
                is_control_plane=False,
            ),
            "node3.testdomain.fqdn.long": model.NodeInfo(
                ipv4="192.0.2.3",
                ipv6="fd00::3",
                is_control_plane=True,
            ),
            "node4.testdomain.fqdn.long": model.NodeInfo(
                ipv4="192.0.2.4",
                ipv6="fd00::4",
                is_control_plane=False,
            ),
        }

        inventory = self._cycle(nodes)

        self.assertIn(
            "\n[masters]\n"
            "node1 ansible_host=192.0.2.1 local_ipv4_address=192.0.2.1"
            " local_ipv6_address=fd00::1 ansible_user=centos\n"
            "node3.testdomain.fqdn.long ansible_host=192.0.2.3"
            " local_ipv4_address=192.0.2.3"
            " local_ipv6_address=fd00::3 ansible_user=centos\n",
            inventory,
        )

        self.assertIn(
            "\n[workers]\n"
            "node2 ansible_host=192.0.2.2 local_ipv4_address=192.0.2.2"
            " local_ipv6_address=fd00::2 ansible_user=centos\n"
            "node4.testdomain.fqdn.long ansible_host=192.0.2.4"
            " local_ipv4_address=192.0.2.4"
            " local_ipv6_address=fd00::4 ansible_user=centos\n",
            inventory,
        )

    def test_mix_of_nodes_v6_only(self):
        nodes = {
            "node1": model.NodeInfo(
                ipv4=None,
                ipv6="fd00::1",
                is_control_plane=True,
            ),
            "node2": model.NodeInfo(
                ipv4=None,
                ipv6="fd00::2",
                is_control_plane=False,
            ),
            "node3.testdomain.fqdn.long": model.NodeInfo(
                ipv4=None,
                ipv6="fd00::3",
                is_control_plane=True,
            ),
            "node4.testdomain.fqdn.long": model.NodeInfo(
                ipv4=None,
                ipv6="fd00::4",
                is_control_plane=False,
            ),
        }

        inventory = self._cycle(nodes)

        self.assertIn(
            "\n[masters]\n"
            "node1 ansible_host=fd00::1 local_ipv6_address=fd00::1"
            " ansible_user=centos\n"
            "node3.testdomain.fqdn.long ansible_host=fd00::3"
            " local_ipv6_address=fd00::3"
            " ansible_user=centos\n",
            inventory,
        )

        self.assertIn(
            "\n[workers]\n"
            "node2 ansible_host=fd00::2 local_ipv6_address=fd00::2"
            " ansible_user=centos\n"
            "node4.testdomain.fqdn.long ansible_host=fd00::4"
            " local_ipv6_address=fd00::4"
            " ansible_user=centos\n",
            inventory,
        )


class Test_reset_cluster_repository(unittest.TestCase):
    def test_calls_git_in_the_correct_sequence(self):
        sp = unittest.mock.Mock()

        def output_gen(cmd, **kwargs):
            if cmd[:2] == ["git", "rev-parse"]:
                return b"remote-name/the-remote-tracking-branch\n"
            raise NotImplementedError

        with contextlib.ExitStack() as stack:
            stack.enter_context(unittest.mock.patch(
                "subprocess.check_call", new=sp.check_call,
            ))

            check_output = stack.enter_context(unittest.mock.patch(
                "subprocess.check_output", new=sp.check_output,
            ))
            check_output.side_effect = output_gen

            deploy._reset_cluster_repository(sentinel.path)

        self.assertSequenceEqual(
            [
                unittest.mock.call.check_output(
                    ["git", "rev-parse", "--abbrev-ref",
                     "--symbolic-full-name", "@{u}"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "fetch", "remote-name"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "reset", "--hard",
                     "remote-name/the-remote-tracking-branch"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "clean", "-f", "."],
                    cwd=sentinel.path,
                ),
            ],
            sp.mock_calls,
        )

    def test_does_not_fetch_if_no_upstream(self):
        sp = unittest.mock.Mock()

        def output_gen(cmd, **kwargs):
            if cmd[:2] == ["git", "rev-parse"]:
                raise subprocess.CalledProcessError(128, cmd)
            raise NotImplementedError

        with contextlib.ExitStack() as stack:
            stack.enter_context(unittest.mock.patch(
                "subprocess.check_call", new=sp.check_call,
            ))

            check_output = stack.enter_context(unittest.mock.patch(
                "subprocess.check_output", new=sp.check_output,
            ))
            check_output.side_effect = output_gen

            deploy._reset_cluster_repository(sentinel.path)

        self.assertSequenceEqual(
            [
                unittest.mock.call.check_output(
                    ["git", "rev-parse", "--abbrev-ref",
                     "--symbolic-full-name", "@{u}"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "checkout", "."],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "clean", "-f", "."],
                    cwd=sentinel.path,
                ),
            ],
            sp.mock_calls,
        )

    def test_ignores_returncode_128_on_fetch(self):
        sp = unittest.mock.Mock()

        def output_gen(cmd, **kwargs):
            if cmd[:2] == ["git", "rev-parse"]:
                return b"remote-name/the-remote-tracking-branch\n"
            raise NotImplementedError

        def call_gen(cmd, **kwargs):
            if cmd[:2] == ["git", "fetch"]:
                raise subprocess.CalledProcessError(128, cmd)

        with contextlib.ExitStack() as stack:
            check_call = stack.enter_context(unittest.mock.patch(
                "subprocess.check_call", new=sp.check_call,
            ))
            check_call.side_effect = call_gen

            check_output = stack.enter_context(unittest.mock.patch(
                "subprocess.check_output", new=sp.check_output,
            ))
            check_output.side_effect = output_gen

            deploy._reset_cluster_repository(sentinel.path)

        self.assertSequenceEqual(
            [
                unittest.mock.call.check_output(
                    ["git", "rev-parse", "--abbrev-ref",
                     "--symbolic-full-name", "@{u}"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "fetch", "remote-name"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "reset", "--hard",
                     "remote-name/the-remote-tracking-branch"],
                    cwd=sentinel.path,
                ),
                unittest.mock.call.check_call(
                    ["git", "clean", "-f", "."],
                    cwd=sentinel.path,
                ),
            ],
            sp.mock_calls,
        )


class Test_write_extra_vars(unittest.TestCase):
    def test_writes_networking_config(self):
        info = unittest.mock.Mock([])
        info.api_server_vrrp_ip = "the vrrp ip"
        info.api_server_frontend_ip = "the frontend ip"

        buf = io.StringIO()

        deploy._write_extra_vars(buf, "cluster-name", info)

        data = json.loads(buf.getvalue())
        self.assertEqual(data["networking_fixed_ip"], "the vrrp ip")
        self.assertEqual(data["networking_floating_ip"], "the frontend ip")

    def test_omits_ip_config_if_none(self):
        info = unittest.mock.Mock([])
        info.api_server_vrrp_ip = None
        info.api_server_frontend_ip = None

        buf = io.StringIO()

        deploy._write_extra_vars(buf, "cluster-name", info)

        data = json.loads(buf.getvalue())
        self.assertNotIn("networking_fixed_ip", data)
        self.assertNotIn("networking_floating_ip", data)


class Testprepare_cluster_repository(unittest.TestCase):
    def setUp(self):
        self.tempdir_cm = tempfile.TemporaryDirectory()
        self.tempdir = self.tempdir_cm.__enter__()
        self.cluster_repository = self.tempdir

    def tearDown(self):
        self.tempdir_cm.__exit__(None, None, None)

    def test_writes_inventory(self):
        cluster_info = model.ClusterInfo(
            api_server_vrrp_ip="192.0.2.1",
            api_server_frontend_ip="192.0.2.2",
            active_nodes=sentinel.nodes,
        )

        def inventory_writer(fout, _):
            fout.write("this inventory was totally written")

        with contextlib.ExitStack() as stack:
            _write_inventory = stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._write_inventory"
            ))
            _write_inventory.side_effect = inventory_writer

            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._reset_cluster_repository"
            ))

            stack.enter_context(unittest.mock.patch(
                "subprocess.check_call"
            ))

            deploy.prepare_cluster_repository(
                "some-cluster",
                self.cluster_repository,
                cluster_info,
            )

        _write_inventory.assert_called_once_with(
            unittest.mock.ANY,
            sentinel.nodes,
        )

        inventory = (
            pathlib.Path(self.cluster_repository) / "inventory" /
            "02_trampoline" / "hosts"
        ).read_text()
        self.assertEqual(inventory, "this inventory was totally written")

        self.assertEqual(
            (pathlib.Path(self.cluster_repository) / "inventory" /
             "03_k8s_base" / "hosts").readlink(),
            pathlib.Path("../02_trampoline/hosts"),
        )

    def test_writes_ip_addresses(self):
        cluster_info = model.ClusterInfo(
            api_server_vrrp_ip="192.0.2.1",
            api_server_frontend_ip="192.0.2.2",
            active_nodes=sentinel.nodes,
        )

        with contextlib.ExitStack() as stack:
            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._write_inventory"
            ))

            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._reset_cluster_repository"
            ))

            stack.enter_context(unittest.mock.patch(
                "subprocess.check_call"
            ))

            deploy.prepare_cluster_repository(
                "some-cluster",
                self.cluster_repository,
                cluster_info,
            )

        extra_cfg = json.loads((
            pathlib.Path(self.cluster_repository) / "inventory" /
            "02_trampoline" / "group_vars" / "all" /
            "terraform_metal-controller.json"
        ).read_text())

        subset = {
            "dualstack_support": False,
            "networking_fixed_ip": "192.0.2.1",
            "networking_floating_ip": "192.0.2.2",
        }
        self.assertEqual(extra_cfg, extra_cfg | subset)

        self.assertEqual(
            (pathlib.Path(self.cluster_repository) / "inventory" /
             "03_k8s_base" / "group_vars" / "all" /
             "terraform_metal-controller.json"
             ).readlink(),
            pathlib.Path(
                "../../../02_trampoline/group_vars/all/"
                "terraform_metal-controller.json"
            ),
        )

    def test_writes_continuous_join_key_info(self):
        cluster_info = model.ClusterInfo(
            api_server_vrrp_ip="192.0.2.1",
            api_server_frontend_ip="192.0.2.2",
            active_nodes=sentinel.nodes,
        )

        with contextlib.ExitStack() as stack:
            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._write_inventory"
            ))

            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._reset_cluster_repository"
            ))

            stack.enter_context(unittest.mock.patch(
                "subprocess.check_call"
            ))

            deploy.prepare_cluster_repository(
                "some-cluster",
                self.cluster_repository,
                cluster_info,
            )

        extra_cfg = json.loads((
            pathlib.Path(self.cluster_repository) / "inventory" /
            "02_trampoline" / "group_vars" / "all" /
            "terraform_metal-controller.json"
        ).read_text())

        subset = {
            "k8s_continuous_join_key_enabled": True,
            "k8s_continuous_join_key_vault_token_script":
                "/var/lib/yaook-cloud-init/vault-login.sh",
            "k8s_continuous_join_key_vault_path":
                "yaook/some-cluster/kv/k8s/join-key",
            "k8s_continuous_join_key_env_path": "/etc/vault-client/config",
        }
        self.assertEqual(extra_cfg, extra_cfg | subset)

        self.assertEqual(
            (pathlib.Path(self.cluster_repository) / "inventory" /
             "03_k8s_base" / "group_vars" / "all" /
             "terraform_metal-controller.json"
             ).readlink(),
            pathlib.Path(
                "../../../02_trampoline/group_vars/all/"
                "terraform_metal-controller.json"
            ),
        )

    def test_calls_update_inventory_before_writing_own_config(self):
        cluster_info = model.ClusterInfo(
            api_server_vrrp_ip="192.0.2.1",
            api_server_frontend_ip="192.0.2.2",
            active_nodes=sentinel.nodes,
        )

        def checked_call(cmd, **kwargs):
            if len(cmd) >= 2 and cmd[1] == "update_inventory.py":
                inventory_base = \
                    pathlib.Path(self.cluster_repository) / "inventory"

                for dirname in ["02_trampoline", "03_k8s_base"]:
                    self.assertFalse(
                        (inventory_base / dirname /
                         "group_vars" / "all" /
                         "terraform_metal-controller.json").exists()
                    )

        with contextlib.ExitStack() as stack:
            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._write_inventory"
            ))

            stack.enter_context(unittest.mock.patch(
                "metal_controller.deploy._reset_cluster_repository"
            ))

            check_call = stack.enter_context(unittest.mock.patch(
                "subprocess.check_call"
            ))
            check_call.side_effect = checked_call

            deploy.prepare_cluster_repository(
                "some-cluster",
                self.cluster_repository,
                cluster_info,
            )

        self.assertIn(
            unittest.mock.call(
                [sys.executable, "managed-k8s/actions/update_inventory.py"],
                cwd=self.cluster_repository,
            ),
            check_call.mock_calls,
        )

        extra_cfg = json.loads((
            pathlib.Path(self.cluster_repository) / "inventory" /
            "02_trampoline" / "group_vars" / "all" /
            "terraform_metal-controller.json"
        ).read_text())

        subset = {
            "dualstack_support": False,
            "networking_fixed_ip": "192.0.2.1",
            "networking_floating_ip": "192.0.2.2",
        }
        self.assertEqual(extra_cfg, extra_cfg | subset)

        self.assertEqual(
            (pathlib.Path(self.cluster_repository) / "inventory" /
             "03_k8s_base" / "group_vars" / "all" /
             "terraform_metal-controller.json"
             ).readlink(),
            pathlib.Path(
                "../../../02_trampoline/group_vars/all/"
                "terraform_metal-controller.json"
            ),
        )


class TestDeploy(unittest.TestCase):
    @unittest.mock.patch.dict(os.environ, {
                                            "YAOOK_DEPLOY_PUBLIC_KEYS": "1key",
                                            "YAOOK_DEPLOY_CONFIGDRIVE_DEBUG": "False",  # noqa:E501
                                            "YAOOK_DEPLOY_IMAGE_CHECKSUM": "34567890",  # noqa:E501
                                            "YAOOK_DEPLOY_IMAGE": "regular.deploy.image:80/image.qcow1",  # noqa:E501
                                            "YAOOK_CLUSTER_MAP": "doener: bude",  # noqa:E501
                                            "VAULT_ADDR": "1.2.3.4",
                                            "VAULT_CAPATH": "path/to/path"
                                           }, clear=True)
    def test_image_config_context(self):
        with contextlib.ExitStack() as stack:
            assemble_user_data = stack.enter_context(
                unittest.mock.patch.object(
                    configdrive, "assemble_user_data",
                ))
            assemble_user_data.return_value = sentinel.user_data,

            pathobject = stack.enter_context(
                unittest.mock.patch.object(
                    pathlib, "Path",
                ))
            pathobject.read_text = unittest.mock.MagicMock()
            pathobject.read_text.return_value = sentinel.text

            assemble_meta_data = stack.enter_context(
                unittest.mock.patch.object(
                    configdrive, "assemble_meta_data",
                ))
            assemble_meta_data.return_value = sentinel.meta_data,

            generate_base_script = stack.enter_context(
                unittest.mock.patch.object(
                    configdrive, "generate_base_script",
                ))
            generate_base_script.return_value = sentinel.meta_data,

            tarball_mock = unittest.mock.MagicMock()
            wrap_tarball = stack.enter_context(
                unittest.mock.patch.object(
                    configdrive, "wrap_tarball",
                ))
            wrap_tarball.return_value = tarball_mock
            # sentinel.tarball.__len__ = lambda x: 0

            assemble_configdrive = stack.enter_context(
                unittest.mock.patch.object(
                    configdrive, "assemble_configdrive",
                ))
            assemble_configdrive.return_value = sentinel.configdrive

            vault_create_server = stack.enter_context(
                unittest.mock.patch.object(
                    deploy.vault, "create_server"
                ))
            vault_create_server.return_value = (
                sentinel.role_id,
                sentinel.wrapping_token
            )

            result = deploy.plan_node(
                metal_controller_deployment_mode=model.
                MetalControllerDeployMethod.NO_ADDITIONAL_DEPLOYMENT,
                network_config=sentinel.network_config,
                hostname=sentinel.device_name,
                cluster=sentinel.cluster,
                cluster_info=sentinel.cluster_info,
                uuid=sentinel.uuid,
                host_vars={},
                is_control_plane=False,
                config_context={
                                    "image_checksum": "17417268941268941298",
                                    "image_source": "happy.image:80/image.qcow3"  # noqa:E501
                                }
            )

        self.assertEqual(
            deploy.NodePlan(
                configdrive=tarball_mock,
                image_checksum="17417268941268941298",
                image_source="happy.image:80/image.qcow3"
            ),
            result
        )
