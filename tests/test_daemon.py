import dataclasses
import logging
import unittest
import unittest.mock
from unittest.mock import sentinel, ANY

import metal_controller.backend as backend
import metal_controller.model as model
import metal_controller.daemon as daemon


class Testsync_node_state(unittest.TestCase):
    def setUp(self):
        self.logger = unittest.mock.Mock(logging.Logger)

        self.backend = unittest.mock.Mock(backend.Backend)
        self.backend.enroll.side_effect = NotImplementedError
        self.backend.manage.side_effect = NotImplementedError

        self.conn = unittest.mock.Mock(["baremetal",
                                        "baremetal_introspection"])

        self.node = unittest.mock.Mock()
        self.node.last_error = None
        self.node.id = sentinel.node_id
        self.node.extra = {"preserved_unrelated_key": sentinel.preserved}

        self.brief = model.NodeBrief(
            id_=sentinel.node_id,
            name=sentinel.name,
            provision_state=sentinel.provision_state,
            backend_node_id=sentinel.backend_node_id,
            intended_state=sentinel.intended_state,
            ignored=sentinel.ignored,
        )

    def test_sync_state_is_noop_if_intended_state_is_none(self):
        self.brief = dataclasses.replace(
            self.brief,
            intended_state=None,
        )

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        self.assertIs(result, self.brief)
        self.assertCountEqual([], self.conn.mock_calls)
        self.assertCountEqual([], self.node.mock_calls)
        self.assertCountEqual([], self.backend.mock_calls)

    def test_sync_state_reports_failure_if_manage_failed(self):
        self.brief = dataclasses.replace(
            self.brief,
            intended_state="available",
            provision_state="enroll",
        )
        self.node.last_error = "some error message"

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        self.conn.baremetal.update_node.assert_called_once_with(
            sentinel.node_id,
            extra={
                "preserved_unrelated_key": sentinel.preserved,
                model.EXTRA_KEY_BACKEND_NODE_ID: self.brief.backend_node_id,
                "metal_controller_deployment_method": ANY,
            },
        )
        self.backend.failure.assert_called_once_with(
            self.brief,
            "some error message",
        )
        self.assertEqual(result.intended_state, None)

    def test_sync_state_is_noop_when_verification_in_progress(self):
        self.brief = dataclasses.replace(
            self.brief,
            intended_state="available",
            provision_state="verifying",
        )

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        self.assertIs(result, self.brief)
        self.assertCountEqual([], self.conn.mock_calls)
        self.assertCountEqual([], self.node.mock_calls)
        self.assertCountEqual([], self.backend.mock_calls)

    def test_sync_state_triggers_provide(self):
        self.brief = dataclasses.replace(
            self.brief,
            intended_state="available",
            provision_state="manageable",
        )

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        self.conn.baremetal.set_node_provision_state.assert_called_once_with(
            sentinel.node_id,
            target="provide",
        )

        self.assertCountEqual([], self.node.mock_calls)
        self.assertCountEqual([], self.backend.mock_calls)
        # ensure that the state is not manageable anymore, otherwise this will
        # cause a double-manage
        self.assertEqual(result.provision_state, "cleaning")

        self.brief = dataclasses.replace(
            self.brief,
            intended_state="available",
            provision_state="available",
        )

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        def test_sync_state_reports_success_if_provide_succeded(self):
            self.conn.baremetal.update_node.assert_called_once_with(
                sentinel.node_id,
                extra={
                    "preserved_unrelated_key": sentinel.preserved,
                    model.EXTRA_KEY_BACKEND_NODE_ID:
                    self.brief.backend_node_id,
                 },
            )

            self.backend.success.assert_called_once_with(self.brief)

            self.assertCountEqual([], self.node.mock_calls)
            self.assertIsNone(result.intended_state)

    def test_sync_state_reports_failure_if_provide_failed(self):
        self.brief = dataclasses.replace(
            self.brief,
            intended_state="available",
            provision_state="clean failed",
        )
        self.node.last_error = sentinel.last_error

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        self.conn.baremetal.update_node.assert_called_once_with(
            sentinel.node_id,
            extra={
                "preserved_unrelated_key": sentinel.preserved,
                model.EXTRA_KEY_BACKEND_NODE_ID: self.brief.backend_node_id,
                "metal_controller_deployment_method": ANY,
            },
        )
        self.backend.failure.assert_called_once_with(
            self.brief,
            sentinel.last_error,
        )
        self.assertCountEqual([], self.node.mock_calls)
        self.assertIsNone(result.intended_state)

    def test_sync_state_is_noop_when_deploying(self):
        self.brief = dataclasses.replace(
            self.brief,
            intended_state="active",
            provision_state="wait call-back",
        )

        result = daemon.sync_node_state(
            self.logger,
            self.conn,
            self.brief,
            self.node,
            self.backend,
        )

        self.assertIs(result, self.brief)
        self.assertCountEqual([], self.conn.mock_calls)
        self.assertCountEqual([], self.node.mock_calls)
        self.assertCountEqual([], self.backend.mock_calls)


class Testorchestrate_node(unittest.TestCase):
    def setUp(self):
        self.logger = unittest.mock.Mock(logging.Logger)

        self.backend = unittest.mock.Mock(backend.Backend)
        self.backend.enroll.side_effect = NotImplementedError
        self.backend.manage.side_effect = NotImplementedError

        self.conn = unittest.mock.Mock(["baremetal",
                                        "baremetal_introspection"])

        self.node = unittest.mock.Mock()
        self.node.is_maintenance = False

        self.brief_from_os = unittest.mock.Mock([])
        self.brief = unittest.mock.Mock([
            "id_",
            "name",
            "provision_state",
            "backend_node_id",
            "intended_state",
            "ignored",
        ])
        self.brief.id_ = self.node.id
        self.brief.name = self.node.name
        self.brief_from_os.return_value = sentinel.dummy_brief

        self.sync_node_state = unittest.mock.Mock([])

        def checked_sync_node_state(logger, conn, brief, node, backend):
            self.assertIs(conn, self.conn)
            self.assertIs(brief, sentinel.dummy_brief)
            self.assertIs(node, self.node)
            self.assertIs(backend, self.backend)
            return self.brief

        self.sync_node_state.side_effect = checked_sync_node_state

        self.__patches = [
            unittest.mock.patch(
                "metal_controller.model.NodeBrief.from_openstack",
                new=self.brief_from_os,
            ),
            unittest.mock.patch(
                "metal_controller.daemon.sync_node_state",
                new=self.sync_node_state,
            ),
        ]

        for p in self.__patches:
            p.start()

    def tearDown(self):
        for p in reversed(self.__patches):
            p.stop()

    def test_calls_enroll_and_returns_action_for_fresh_node(self):
        self.brief.provision_state = "enroll"
        self.brief.intended_state = None
        self.brief.ignored = False

        self.backend.enroll.side_effect = None
        self.backend.enroll.return_value = sentinel.action
        self.conn.baremetal_introspection.get_introspection_data.return_value = sentinel.introspection  # noqa:E501

        result = daemon.orchestrate_node(
            self.logger,
            self.conn,
            self.node,
            self.backend,
        )

        self.brief_from_os.assert_called_once_with(self.node)
        self.backend.enroll.assert_called_once_with(
            self.brief,
            sentinel.introspection,
        )

        self.assertEqual(result, sentinel.action)

    def test_calls_enroll_and_returns_action_for_manageable_node(self):
        self.brief.provision_state = "manageable"
        self.brief.intended_state = None
        self.brief.ignored = False

        self.backend.enroll.side_effect = None
        self.backend.enroll.return_value = sentinel.action
        self.conn.baremetal_introspection.get_introspection_data.return_value = sentinel.introspection  # noqa:E501

        result = daemon.orchestrate_node(
            self.logger,
            self.conn,
            self.node,
            self.backend,
        )

        self.brief_from_os.assert_called_once_with(self.node)
        self.backend.enroll.assert_called_once_with(
            self.brief,
            sentinel.introspection,
        )

        self.assertEqual(result, sentinel.action)

    def test_ignores_node_in_maintenance(self):
        self.brief.provision_state = "enroll"
        self.brief.intended_state = None
        self.brief.ignored = False

        self.node.is_maintenance = True

        self.backend.enroll.side_effect = None
        self.backend.enroll.return_value = sentinel.action
        self.conn.baremetal_introspection.get_introspection_data.return_value = sentinel.introspection  # noqa:E501

        result = daemon.orchestrate_node(
            self.logger,
            self.conn,
            self.node,
            self.backend,
        )

        self.brief_from_os.assert_not_called()

        self.assertCountEqual([], self.backend.mock_calls)
        self.assertCountEqual([], self.conn.mock_calls)
        self.assertIsNone(result)

    def test_is_noop_for_random_provision_state(self):
        self.brief.provision_state = "foobar"
        self.brief.intended_state = None
        self.brief.ignored = False

        self.backend.enroll.side_effect = None
        self.backend.enroll.return_value = sentinel.action
        self.conn.baremetal_introspection.get_introspection_data.return_value = sentinel.introspection  # noqa:E501

        result = daemon.orchestrate_node(
            self.logger,
            self.conn,
            self.node,
            self.backend,
        )

        self.brief_from_os.assert_called_once_with(self.node)

        self.assertCountEqual([], self.backend.mock_calls)
        self.assertCountEqual([], self.conn.mock_calls)
        self.assertIsNone(result)

    def test_calls_manage_and_returns_action_for_available(self):
        self.brief.provision_state = "available"
        self.brief.intended_state = None
        self.brief.ignored = False

        self.backend.manage.side_effect = None
        self.backend.manage.return_value = sentinel.action
        self.conn.baremetal_introspection.get_introspection_data.return_value = sentinel.introspection  # noqa:E501

        result = daemon.orchestrate_node(
            self.logger,
            self.conn,
            self.node,
            self.backend,
        )

        self.brief_from_os.assert_called_once_with(self.node)
        self.backend.manage.assert_called_once_with(
            self.brief,
            sentinel.introspection,
        )

        self.assertEqual(result, sentinel.action)
