import unittest
import unittest.mock
from unittest.mock import sentinel

import metal_controller.vault as vault


class Testget_client(unittest.TestCase):
    @unittest.mock.patch.dict("os.environ", {
        "VAULT_TOKEN": "foobar",
    })
    @unittest.mock.patch("hvac.Client")
    def test_falls_back_to_token_if_no_approle_creds_present(
            self,
            client_ctor):
        client = sentinel.client
        client_ctor.return_value = client

        with vault.get_client() as result:
            client_ctor.assert_called_once_with()
            self.assertEqual(result, client)

    @unittest.mock.patch.dict("os.environ", {
        "VAULT_ROLE_ID": "the-role-id",
        "VAULT_SECRET_ID": "the-secret-id",
        "VAULT_AUTH_PATH": "foo/",
    })
    @unittest.mock.patch("hvac.Client")
    def test_logs_in_using_approle_if_available(
            self,
            client_ctor):
        client = unittest.mock.Mock()
        client.auth.token.lookup_self.return_value = {
            "data": {"accessor": sentinel.token_accessor},
        }
        client_ctor.return_value = client

        with vault.get_client() as result:
            client_ctor.assert_called_once_with()
            client.auth.approle.login.assert_called_once_with(
                "the-role-id",
                "the-secret-id",
                mount_point="foo/"
            )
            self.assertEqual(result, client)

    @unittest.mock.patch.dict("os.environ", {
        "VAULT_ROLE_ID": "the-role-id",
        "VAULT_SECRET_ID": "the-secret-id",
        "VAULT_AUTH_PATH": "foo/",
    })
    @unittest.mock.patch("hvac.Client")
    def test_revokes_approle_token_at_exit(
            self,
            client_ctor):
        client = unittest.mock.Mock()

        # earlier versions of the code used lookup self we still mock
        # it to make sure it is no longer called!
        client.auth.token.lookup_self.return_value = {
            "data": {"accessor": sentinel.token_accessor},
        }
        client_ctor.return_value = client

        with vault.get_client() as result:
            client_ctor.assert_called_once_with()
            client.auth.approle.login.assert_called_once_with(
                "the-role-id",
                "the-secret-id",
                mount_point="foo/"
            )
            client.auth.token.revoke_self.assert_not_called()
            self.assertEqual(result, client)

        client.auth.token.lookup_self.assert_not_called()
        client.auth.token.revoke_self.assert_called_once_with()

    @unittest.mock.patch.dict("os.environ", {
        "VAULT_ROLE_ID": "the-role-id",
        "VAULT_SECRET_ID": "the-secret-id",
        "VAULT_AUTH_PATH": "foo/",
    })
    @unittest.mock.patch("hvac.Client")
    def test_revokes_approle_token_at_exception(
            self,
            client_ctor):
        class FooException(Exception):
            pass

        client = unittest.mock.Mock()

        # earlier versions of the code used lookup self we still mock
        # it to make sure it is no longer called!
        client.auth.token.lookup_self.return_value = {
            "data": {"accessor": sentinel.token_accessor},
        }

        client_ctor.return_value = client

        with self.assertRaises(FooException):
            with vault.get_client():
                raise FooException()

        client.auth.token.lookup_self.assert_not_called()
        client.auth.token.revoke_self.assert_called_once_with()
