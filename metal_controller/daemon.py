import dataclasses
import logging
import time
import typing

import openstack
from openstack.connection import Connection as OSConnection
from openstack.baremetal.v1.node import Node as BaremetalNode

from .model import NodeBrief, update_extra_attributes
from .backend import Backend, Action


def sync_node_state(
        logger: logging.Logger,
        conn: OSConnection,
        brief: NodeBrief,
        node: BaremetalNode,
        backend: Backend,
        ) -> NodeBrief:
    """
    Synchronize the node state.

    This obtains a summary of the node state and judges it based on the
    intended and the actual state.

    If the current state is not a valid predecessor of the intended state, a
    failure is reported to the backend and the intended state is cleared.

    If the current state is a valid successor of or equal to the intended
    state, success is reported to the backend and the intended state is
    cleared.

    Changes to the intended state are applied to the returned copy of the given
    :class:`~.NodeBrief` instance as well as the live object in the baremetal
    API.
    """
    logger.debug(
        "synchronizing node %s: provision state = %s, intended state = %s, "
        "error = %s",
        brief.id_,
        brief.provision_state,
        brief.intended_state,
        node.last_error,
    )

    if brief.intended_state == "available":
        if (
                (brief.provision_state == "enroll" and
                 node.last_error is not None) or
                brief.provision_state == "clean failed"):
            logger.warning(
                "failed to move node %s to available state "
                "(provision state: %r), reporting error: %s",
                brief.id_,
                node.provision_state,
                node.last_error,
            )
            # enroll on provide failed
            backend.failure(
                brief,
                node.last_error,
            )
            brief = dataclasses.replace(
                brief,
                intended_state=None,
            )
            update_extra_attributes(
                conn,
                node,
                backend_node_id=brief.backend_node_id,
                intended_state=brief.intended_state,
                last_message=None,
            )
        elif brief.provision_state == "manageable":
            logger.info("node %s completed verification, making it available",
                        brief.id_)
            # proceed
            conn.baremetal.set_node_provision_state(
                node.id,
                target="provide",
            )
            brief = dataclasses.replace(brief, provision_state="cleaning")
        elif brief.provision_state == "available":
            logger.info("node %s completed cleaning, it is now available",
                        brief.id_)
            # yay
            backend.success(brief)
            brief = dataclasses.replace(
                brief,
                intended_state=None,
            )
            update_extra_attributes(
                conn,
                node,
                backend_node_id=brief.backend_node_id,
                intended_state=brief.intended_state,
                last_message=None,
            )
        else:
            logger.debug("node %s is still progressing...", brief.id_)
    return brief


def orchestrate_node(
        logger: logging.Logger,
        conn: OSConnection,
        node: BaremetalNode,
        backend: Backend,
        ) -> typing.Optional[Action]:
    if node.is_maintenance:
        logger.info("ignoring node %s which is in maintenance state",
                    node.id)
        return None

    brief = sync_node_state(
        logger,
        conn,
        NodeBrief.from_openstack(node),
        node,
        backend,
    )
    if brief.provision_state in ["enroll", "manageable"]:
        logger.debug("node %s is in %s state, inspecting and asking backend",
                     brief.id_,
                     brief.provision_state)
        try:
            introspection = get_introspection_data(conn, node.id)
        except IntrospectionNotFound:
            logger.warning("No introspection data found for node: %s."
                           " Skipping", node.id)
            return None
        try:
            return backend.enroll(brief, introspection)
        except Exception:
            logger.exception("backend failed to enroll node %s", node.id)
            return None
    elif brief.provision_state in ["available"]:
        logger.debug("node %s is in %s state, inspecting and asking backend",
                     brief.id_,
                     brief.provision_state)
        try:
            introspection = get_introspection_data(conn, node.id)
        except IntrospectionNotFound:
            logger.warning("No introspection data found for node: %s."
                           " Skipping", node.id)
            return None
        try:
            return backend.manage(brief, introspection)
        except Exception:
            logger.exception("backend failed to manage node %s", node.id)
            return None

    return None


def get_introspection_data(
        osclient: OSConnection,
        node_id: str
        ) -> typing.Any:
    try:
        introspection = osclient.baremetal_introspection.\
            get_introspection_data(node_id)
    except openstack.exceptions.ResourceNotFound:
        raise IntrospectionNotFound
    return introspection


class IntrospectionNotFound(Exception):
    pass


class Daemon:
    def __init__(
            self,
            interval: int,
            backend: Backend,
            logger: logging.Logger):
        super().__init__()
        self._interval = interval
        self._backend = backend
        self._conn = openstack.connect()
        self.logger = logger

    def _sync(self):
        for node in self._conn.baremetal.nodes():
            node = self._conn.baremetal.get_node(node.id)
            action = orchestrate_node(
                self.logger,
                self._conn,
                node,
                self._backend,
            )

            if action is not None:
                self.logger.debug(
                    "orchestrator returned action %s for node %s, applying",
                    action.KIND, node.id,
                )
                try:
                    action.apply_to(
                        self._conn,
                        node,
                    )
                except Exception as exc:
                    self.logger.exception(
                        "failed to apply action %s to node %s",
                        action.KIND,
                        node.id,
                    )
                    self._backend.failure(
                        NodeBrief.from_openstack(node),
                        str(exc),
                    )
            else:
                self.logger.debug(
                    "orchestrator returned no action for node %s, skipping",
                    node.id,
                )

    def run(self):
        while True:
            self.logger.debug("synchronizing nodes")
            self._sync()
            time.sleep(self._interval)
