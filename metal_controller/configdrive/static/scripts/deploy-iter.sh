#!/bin/bash
set -euo pipefail
umask 0027
skiptags="$(jq -r .ds.meta_data.yaook_deploy_skip_tags /run/cloud-init/instance-data-sensitive.json || echo 'dontskipanytagsorryforthis')"
node_role="$(jq -r .ds.meta_data.node_role /run/cloud-init/instance-data-sensitive.json)"
export AFLAGS="-c local -l $(hostname) --skip-tags ssh-known-hosts,detect-user,$(printf '%q' "$skiptags")"

. /etc/vault-client/config
export VAULT_ADDR VAULT_CAPATH
mkdir -p inventory/.etc

function revoke() {
    vault token revoke -self 2>/dev/null || true
}

trap revoke EXIT

function download() {
    local prefix="$1"
    local dir="$2"

    # check if k8s cluster kv store includes any data
    if [[ "$(vault kv list -format=json "$prefix$dir" | jq -r '.[]')" != "" ]]; then
        vault kv list -format=json "$prefix$dir" | jq -r '.[]' | while read -r name; do
            if [[ "$name" = *'/' ]]; then
                # is a directory
                mkdir -p "$name"
                pushd "$name" >/dev/null
                download "$prefix" "$dir"/"$name"
                popd >/dev/null
            else
                # not a directory
                vault kv get -field=data "$prefix$dir/$name" | base64 -d | sponge "$name"
            fi
        done
    fi
}

deployment="$(cat /etc/vault-client/deployment)"
export VAULT_TOKEN="$(/var/lib/yaook-cloud-init/vault-login.sh)"
pushd inventory/.etc >/dev/null
download "yaook/$deployment/kv/k8s/etc" ''
popd >/dev/null

# and now we forge the join command
set +e
join_key=$(vault kv get -field=token "yaook/$deployment/kv/k8s/join-key")
cacert_hash=$(vault kv get -field=ca_cert_hash "yaook/$deployment/kv/k8s/join-key")
set -e
vault token revoke -self
unset VAULT_TOKEN

if test -n "$join_key" && test -n "$cacert_hash"; then
    printf '%s' "$join_key" > inventory/.etc/kubeadm_token
else
    printf 'No recent join key found\n' >&2
fi

function upload() {
    deployment="$(cat /etc/vault-client/deployment)"
    export VAULT_TOKEN="$(/var/lib/yaook-cloud-init/vault-login.sh)"
    cd inventory/.etc
    find -type f -print0 | cut -z -d'/' -f2- | xargs -0 -n1 -- sh -c 'base64 "$1" | vault kv put "$0"/kv/k8s/etc/"$1" data=-' "yaook/$deployment"
    vault token revoke -self
}

if [ "$node_role" = "control-plane" ]; then
    trap upload EXIT
fi

export VIRTUAL_ENV="$(realpath ../venv)"
export PATH="$(realpath ../venv/bin):$PATH"
export WG_USAGE=false
export TF_USAGE=false

./managed-k8s/actions/apply-stage2.sh
./managed-k8s/actions/apply-stage3.sh
