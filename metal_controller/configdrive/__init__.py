import base64
import copy
import email.mime.base
import email.mime.multipart
import gzip
import importlib.resources
import io
import logging
import os
import pathlib
import subprocess
import tarfile
import tempfile
import typing
import stat
from jinja2 import Template

import yaml

from metal_controller import model


logger = logging.getLogger(__name__)


def _common_filter_func(info):
    *_, name = info.name.rsplit("/", 1)
    if name.startswith("."):
        return None
    if info.mode & stat.S_IXUSR != 0:
        info.mode = 0o700
    else:
        info.mode = 0o600
    logger.debug("including %r in tarball", info.name)
    return info


def _collect_static_files(tarball: tarfile.TarFile) -> None:
    """
    Collect the static files from the package in a given tar file.
    """

    staticdir = (
        importlib.resources.files("metal_controller.configdrive") / "static"
    )
    for entry in staticdir.iterdir():
        tarball.add(
            str(entry),
            arcname=entry.name,
            filter=_common_filter_func,
        )


def _collect_cluster_repo(
        tarball: tarfile.TarFile,
        cluster_repository: pathlib.Path) -> None:
    tarball.add(
        str(cluster_repository),
        arcname="cluster",
        filter=_common_filter_func,
    )


def build_tarball(
        *,
        cluster_repository: pathlib.Path,
        ) -> bytes:
    """
    Collect all static files from the metal_controller package as well as
    a defined subset of the cluster repository tree into a single tarball.

    Returns the tarball base64-encoded.
    """
    buf = io.BytesIO()
    with tarfile.open(fileobj=buf, mode="w:bz2") as tarf:
        _collect_static_files(tarf)
        _collect_cluster_repo(tarf, cluster_repository)

    return base64.b64encode(buf.getvalue())


def assemble_deploy_script(
        tarball: bytes,
        ) -> bytes:
    """
    Assemble the deploy script using the given tarball of payload.
    """
    path = importlib.resources.files("metal_controller.configdrive")
    path = path / "templates"
    path = path / "deploy.sh"
    deploy_script_template = path.read_bytes()

    return deploy_script_template.replace(b"{{ tarball }}", tarball)


def generate_base_script(
        metal_controller_deployment_mode: model.MetalControllerDeployMethod
        ) -> bytes:
    """
    Load the base script
    """
    path = importlib.resources.files("metal_controller.configdrive")
    path = path / "templates"
    template_path = path / "base.sh.j2"
    base_template = Template(template_path.read_text())

    match metal_controller_deployment_mode:
        case model.MetalControllerDeployMethod.YAOOK_K8S:
            path = path / "yaook-k8s-base-part.val"
            value = path.read_text()
            output = base_template.render(base_content=value)
        case model.MetalControllerDeployMethod.TEMPLATE_BASE:
            template = pathlib.Path(
                os.getenv(
                    "YAOOK_TEMPLATE_BASE", "/var/lib/base-template-values"))
            with open(template, 'r') as values:
                output = base_template.render(base_content=values.read())
        case model.MetalControllerDeployMethod.NO_ADDITIONAL_DEPLOYMENT:
            output = base_template.render(base_content="")

    return bytes(output, encoding='utf-8')


def assemble_meta_data(
        *,
        cluster: str,
        uuid: str,
        hostname: str,
        vault_addr: str,
        vault_cacert: str,
        public_keys: typing.Collection[str],
        skip_tags: typing.Collection[str],
        vault_role_id: str,
        vault_wrapped_secret_id: str,
        node_role: str,
        apt_proxy: typing.Optional[str],
        apt_hashicorp_gpg_key: typing.Optional[str],
        apt_hashicorp_repo_url: typing.Optional[str],
        pip_mirror_url: typing.Optional[str],
        pip_mirror_cacert: typing.Optional[str],
        ansible_collections_preload: typing.Optional[str],
        ansible_collections_preload_ca: typing.Optional[str],
        netbox_context_generic_environment: typing.Mapping[str, typing.Any]
        ) -> typing.Mapping[str, typing.Any]:
    meta_data = {
        "uuid": uuid,
        "node_role": node_role,
        "local-hostname": hostname,
        "fqdn": hostname,
        "public-keys": {
            str(i): key
            for i, key in enumerate(public_keys)
        },
        "yaook_deployment": cluster,
        "yaook_vault_addr": vault_addr,
        "yaook_vault_cacert": vault_cacert,
        "yaook_role_id": vault_role_id,
        "yaook_secret_id_wrapping_token": vault_wrapped_secret_id,
    }
    if apt_proxy is not None:
        meta_data["yaook_apt_proxy"] = apt_proxy
    if apt_hashicorp_gpg_key is not None:
        meta_data["yaook_apt_hashicorp_gpg_key"] = apt_hashicorp_gpg_key
    if apt_hashicorp_repo_url is not None:
        meta_data["yaook_apt_hashicorp_repo_url"] = apt_hashicorp_repo_url
    if pip_mirror_url is not None:
        meta_data["yaook_pip_mirror_url"] = pip_mirror_url
    if pip_mirror_cacert is not None:
        meta_data["yaook_pip_mirror_cacert"] = pip_mirror_cacert
    if ansible_collections_preload is not None:
        meta_data["yaook_ansible_collections_preload"] =\
            ansible_collections_preload
    if ansible_collections_preload_ca is not None:
        meta_data["yaook_ansible_collections_preload_ca"] =\
            ansible_collections_preload_ca
    if netbox_context_generic_environment is not None:
        meta_data["yaook_netbox_context_environment"] = \
            copy.deepcopy(netbox_context_generic_environment)
    if skip_tags:
        meta_data["yaook_deploy_skip_tags"] = ",".join(skip_tags)
    return meta_data


def _pack_mime(
        name: str,
        maintype: str,
        subtype: str,
        data: typing.Union[bytes, str]) -> email.mime.base.MIMEBase:
    part = email.mime.base.MIMEBase(maintype, subtype)
    if isinstance(data, bytes):
        part.set_payload(data)
    else:
        part.set_payload(data, charset="utf-8")
    part.add_header("Content-Disposition", "attachment")
    part.set_param("filename", name, header="Content-Disposition")
    return part


def assemble_user_data(
        base_script: bytes,
        cloud_config: typing.Mapping[str, typing.Any],
        ) -> bytes:
    """
    Assemble the user data as multipart MIME object.
    """
    user_data = email.mime.multipart.MIMEMultipart()
    user_data.attach(_pack_mime(
        "cloud-config",
        "text", "cloud-config",
        yaml.safe_dump(cloud_config),
    ))
    user_data.attach(_pack_mime(
        "base.sh",
        "text", "x-shellscript",
        base_script,
    ))
    return user_data.as_bytes()


def assemble_configdrive(
        *,
        meta_data: typing.Mapping[str, typing.Any],
        network_config: typing.Mapping[str, typing.Any],
        user_data: bytes,
        deploy_script: bytes,
        ) -> bytes:
    """
    Assemble the configdrive as raw ISO image.
    """
    with tempfile.TemporaryDirectory() as tempdir:
        path = pathlib.Path(tempdir)
        with (path / "user-data").open("wb") as f:
            f.write(user_data)
        with (path / "meta-data").open("w") as f:
            yaml.safe_dump(meta_data, f)
        with (path / "network-config").open("w") as f:
            yaml.safe_dump(network_config, f, sort_keys=False)
        with (path / "deploy.sh").open("wb") as f:
            f.write(deploy_script)

        subprocess.check_call(
            [
                "mkisofs",
                "-joliet",
                "-rock",
                "-volid", "cidata",
                "-quiet",
                "-output", str(path / "nocloud.iso"),
                "user-data",
                "network-config",
                "meta-data",
                "deploy.sh"
            ],
            cwd=str(path),
        )

        result = (path / "nocloud.iso").read_bytes()

    logger.debug("generated %d bytes of raw ISO", len(result))

    return result


def wrap_tarball(configdrive: bytes) -> str:
    return base64.b64encode(gzip.compress(configdrive)).decode("ascii")
