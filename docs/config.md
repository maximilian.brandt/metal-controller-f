# Environment Variable Reference

## OpenStack Authentication and Service Catalog

openstacksdk supports configuring the authentication as well as endpoints to connect to using environment variables. metal-controller relies on this mechansim in order to connect to OpenStack.

That means you need to set `OS_AUTH_TYPE` and related variables for metal-controller to function.


# MetalController options

- `METAL_CONTROLLER_RECONCILE_INTERVAL` (default: 5): Defines the time between the consecutive reconciliations of all nodes in Ironic and Netbox. Lower intervals can be taxing on the Netbox in environments with large numbers of nodes.

## NetBox Integration

- `NETBOX_URL` (required): URL to the NetBox HTTP(S) service
- `NETBOX_TOKEN` (required): Token to use to authenticate with the NetBox API
- `NETBOX_ERROR_TAG` (default: `yaook--has-orchestration-error`): The NetBox tag with the given slug will be assigned to nodes where an orchestration step has failed.
- `NETBOX_ERROR_TAG_AUTOCREATE` (default: true): If enabled, the error tag will be automatically created if no tag with this slug exists. If you set this to false and no tag with the given slug exists, terrible things will happen.
- `NETBOX_CLUSTER_TYPES` (default: `yaook`): A comma-separated list of Slugs of Cluster Types to consider. Any device which does not belong to a Cluster with a Cluster Type with a Slug from this list will be ignored.
- `NETBOX_CLUSTER_DOMAIN_FIELD` (default: `cf_yaook_domain`): The field to use as FQDN for the cluster. If you intend to use the FQDN as Name field in the cluster, it might make sense to change this to `name` instead. If you do that, the `yaook_domain` custom field is not required at all.
- `NETBOX_GATEWAY_TAG` (default: `yaook--default-gateway`): Tag for IP addresses to use as default gateways for devices in the same prefix.
- `NETBOX_NAMESERVER_FIELD` (default: `yaook_nameserver`): Field for the list of IP addresses to use as nameservers for devices in the prefix.
- `NETBOX_IP_ALLOCATE_TAG` (default: `yaook--ip-auto-assign`): Tag for VLANs in which IP address auto allocation should happen and Prefixes which are eligible as pools for IP address auto allocation.
- `NETBOX_IP_PRIMARY_TAG` (default: `yaook--ip-primary`): Tag for VLANs from which the "primary" IP addresses of a device are sourced. If auto allocation is enabled and an IP address is assigned from a VLAN with this tag, it will also be set as the primary IP of  the device.
- `NETBOX_NODE_MATCH_MODE` (default: `by-interface`): Control the logic by which nodes in NetBox are matched against nodes in Ironic.

    - `by-interface` looks for an attribute on an Interface belonging to the NetBox device
    - `by-attribute` looks for an attribtue on the Device itself

    See the next two options for details.
- `NETBOX_NODE_MATCH_FIELD` (default: `mac_address`): The field of the object which is matched against (an interface or a device, see `NETBOX_NODE_MATCH_MODE`). The value of the field is compared with the result of the `NETBOX_NODE_MATCH_EXPR` expression.
- `NETBOX_NODE_MATCH_EXPR` (default: `{inventory[bmc_mac]}`): A python format expression which should be evaluated for comparing against a field value (see above). Within the format expression, `inventory` contains the `"inventory"` key of the OpenStack Ironic introspection data.
- `NETBOX_MANAGEABLE_STATUSES` (default: `planned,staged,active`): The status values for which the metal controller will attempt to manage a node (enroll in Ironic assign hostname, IP addresses).
- `NETBOX_DEPLOYABLE_STATUSES` (default: `staged,active`): The status values for which the metal controller will attempt to deploy a node. Note: In the future, `staged` may be removed from the default.
- `NETBOX_IPMI_VAULT_MOUNTPOINT_TEMPLATE` (default: `yaook/{cluster}/kv`): A python format expression which is evaluated to find the mountpoint of the key-value store in Vault where IPMI passwords are held. See `NETBOX_IPMI_VAULT_PATH_TEMPLATE` for the available variables.
- `NETBOX_IPMI_VAULT_PATH_TEMPLATE` (default: `ipmi/{device.asset_tag}`): A python format expression which is evaluated to find the path in the key-value store in Vault where the IPMI password for a node is stored. In this expression, `device` is the NetBox Device object and `cluster` is the FQDN of the cluster to which it belongs.
- `NETBOX_IPMI_VAULT_USERNAME_KEY` (default: `username`): The key inside the key-value object from Vault to use as IPMI username.
- `NETBOX_IPMI_VAULT_PASSWORD_KEY` (default: `password`): The key inside the key-value object from Vault to use as IPMI password.

## Vault Integration

- `VAULT_AUTH_PATH` (required, unless `VAULT_TOKEN` is set): Path to the approle authentication method at the vault server.
- `VAULT_ROLE_ID` (required, unless `VAULT_TOKEN` is set): The Role ID to authenticate against.
- `VAULT_SECRET_ID` (required, unless `VAULT_TOKEN` is set): The Secret ID to authenticate against.
- `VAULT_TOKEN` (fallback, not recommended): If no other authentication is available (i.e. if the above variables are missing), the metal controller will fall back to attempt to authenticate using this token.

    As there is no logic for renewing the token, this requires to restart the metal controller regularly with a new token or to use a long-lived token, both of which are not ideal. Using an approle is the recommended approach.

- `VAULT_ADDR` (required): HTTPS URL to the Vault server.
- `VAULT_CAPATH` (required): Path to the CA certificate to verify the Vault API server.

    Note: This CA is also included in the configdrive to bootstrap a nodes' trust with the Vault server.

- `VAULT_POLICY_PREFIX` (default: `yaook`): Prefix for names of policies related to Yaook.
- `VAULT_PATH_PREFIX` (default: `yaook`): Prefix for paths related to Yaook.

## Deployment Configuration

- `YAOOK_CLUSTER_MAP` (required): A YAML document which maps cluster FQDNs to the path of their cluster repository in the local filesystem.
- `YAOOK_DEPLOY_PUBLIC_KEYS` (required): A newline-separated list of public SSH keys to deploy for the default user on each node.
- `YAOOK_DEPLOY_IMAGE` (required): HTTP(S) URL to the image to deploy on each node.
- `YAOOK_DEPLOY_IMAGE_CHECKSUM` (required): MD5 checksum (I refuse to call it a hash nowadays) of the image.
- `YAOOK_DEPLOY_CONFIGDRIVE_DEBUG` (default: false): If set to true, the configdrive used for deploying a node will be dumped to the cluster repository. This is for debugging only and should not be set in production. The config drive contains sensitive information.
- `YAOOK_DEPLOY_VAULT_ADDR` (default: the value of `VAULT_ADDR`): The address under which the nodes can reach the Vault server, if it differs from `$VAULT_ADDR`.
- `YAOOK_TEMPLATE_BASE` (default: '/var/lib/base-template-values'): Specifies the path to a custom Bash script, that will templated into the `base.sh` during the `TEMPLATE_BASE` deployment method. See the [Config Context based options](#config-context-based-options)

# Config Context based options
All passed options need to be configured unter the "metal-controller" key.
The following options can be set on a per node base via config context:
- `deployment_method` (available options: `YAOOK_K8S`, `TEMPLATE_BASE`, `NO_ADDITIONAL_DEPLOYMENT`):
    - `YAOOK_K8S` if not specified, this is the default deployment options of the MetalController
    - `TEMPLATE_BASE` activates the templating of the in the `YAOOK_TEMPLATE_BASE` envvar specified Bash script into the base.sh.
    - `NO_ADDITIONAL_DEPLOYMENT` after deployment of the node, no (yaook/k8s) scripts/services will be executed.
- `deploy_kernel` (only together with deploy_ramdisk) use a dedicated kernel for this node during the deployment steps (e.g. for different Architectures)
- `deploy_ramdisk` (only together with deploy_kernel) use a dedicated ramdisk for this node during the deployment steps (e.g. for different Architectures)
- `image_source` use a different than via env specified image. (the checksum needs to match)
- `image_checksum` use a different than via env specified image. (the image needs to match the checksum)
- `bmc_dns_suffix` if devices are used without access to inband ipmi, provide the bmc dns suffix and the deriving template as constant to do a lookup by DDNS
- `driver_info_override` option to provide key-value pairs that are merged into the driver_info field

example:
```json
{
    "metal-controller": {
        "deployment_method": "YAOOK_K8S / TEMPLATE_BASE / NO_ADDITIONAL_DEPLOYMENT",
        "deploy_kernel": "http://my-kewl-imgserver:80/ipa_arm64.kernel",
        "deploy_ramdisk": "http://my-kewl-imgserver:80/ipa_arm64.initramfs",
        "image_checksum": "7e844435bf28aca2c1d8c1079e05bcbe",
        "image_source": "http://my-kewl-imgserver:80/fedora-69-aarch64.qcow2",
        "bmc_dns_suffix": "all.my.bmc.belong.to.us",
        "driver_info_override": {
            "redfish_system_id": "/redfish/v1/Systems/Self",
        }
    }
}
```
